# Getting Start

## setup
```
npm install sealportal-components
```

## How to used
```javascript
import { Component } from 'sealportal-components';

<Component></Component>
```

## Parameters
parameter | description
--- | ---
domain | api domain url
gallery | img cdn url
custInfo  | account infomation who log in
language | i18n key

```javascript
import { Component } from 'sealportal-components';

<Component
    domain="http://domain:80/api/path"
    custInfo={custInfo}
></Component>
```

## Precautions
Style class name begin with "sealportal_"
```html
<div class="sealportal_fbw-catalogue__product"></div>
```

## Components and size
### Facebook-wall
parameter | description
--- | ---
width | 100% with parent
height  | with children height
font-size | wait design
color | with css variable
background | with css variable

---
# Getting Started with CRA Example

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.
